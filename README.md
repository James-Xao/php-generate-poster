# Postermaker 海报生成器
A poster make base on gd lib. (PHP)  一个用php生成海报的神器

# Usage 用法

比如在文件someaction.php中使用：
```
$poster = new \PosterMaker\Postermaker(800, 1100);
$poster
    ->addImg('./bg.jpg', [0,0], [800, 1100])
    ->addImgAuto('./data/upload/cover.jpg', false,200)
    ->addImgCenter('./data/upload/avatar.png')
    ->addText('php是世界上最好的语言', 24, [30, 910], [255, 255, 255])
    ->addQrCode('http://com/123', [500,800],200 , 10 ,'长按扫码购买')
    ->addQrCodeCenter('http://com/123', [500,800],200 , 10 ,'长按扫码购买' ,12 , [200 , 20])
    ->render();
```

### 安装
```
composer require james-xiao/postermaker:dev-master
```
### addImg
按大小创建一个海报 
```
$poster = new \PosterMaker\Postermaker(800, 1100); // (width, height)
```
### addImg
添加图片 (图片路径, [x坐标, y坐标], [width, height])
```
$poster->addImg('./data/upload/cover.jpg', [30,30],[740, 500])
```
### addImgAuto
添加图片自适应剪裁 (图片路径, $b_w 剪裁后的宽度  , $b_h 剪裁后的高度)
```
$poster->addImg('./data/upload/cover.jpg', false,500)
```
### addImgCenter
添加图片居中 (图片路径, $from 开始坐标  ,  $to 结束 坐标)
```
$poster->addImgCenter('./data/upload/cover.jpg', [30,30],[740, 500])
```
### addText
添加图片 (文字内容, 字体大小, [x坐标, y坐标], 颜色[R,G,B])
```
$poster->addText('php是世界上最好的语言', 30, [30,610], [255, 255, 255])
```
### addQrCode
添加二维码 (文字内容, [x坐标, y坐标], 尺寸 , 白边大小, 文字 ,文字大小 )
```
$poster->addQrCode('http://com/123', [500,800],200,10 ,'长按扫码购买' ,12 )
```
### addQrCodeCenter
添加二维码 指定位置居中 (文字内容, [x坐标, y坐标], 尺寸 , 白边大小, 文字 ,文字大小, $from 开始坐标  ,  $to 结束 坐标 )
```
$poster->addQrCodeCenter('http://com/123', [500,800],200,10 ,'长按扫码购买' ,12,[30,30],[740, 500])
```
### render
添加图片 (文字内容, 字体大小, [x坐标, y坐标], 颜色[R,G,B])
```
$poster->render('./save.png'); // 保持为图片
// or
$poster->render(); // show image in html: `<img src="someaction.php" style="border-radius: 20px;"/>`
```
# Thinkphp等框架里中的使用
因为有些php框架的控制器默认输出html，所以如果在控制器里直接输出图片的话，需要在控制器最后一行加上：
```
return response()->contentType('image/png');
```
或者用exit
```
exit();
```

# Author
James-xiao
Email:406155794@qq.com