<?php
namespace PosterMaker;

class PosterMaker
{
    private $bg; // 背景图
    private $bg_w; // 背景图
    private $bg_h; // 背景图
    private $bg_color;
    /**
     * 构造函数
     * @param $w int 宽度(px)
     * @param $w int 高度(px)
     * @param $bg_color array RGB color value
     */
    public function __construct(int $w, int $h, $bg_color = [100, 150, 150])
    {
        $this->bg_w = $w;
        $this->bg_h = $h;
        $this->bg_color = $bg_color;
        $this->createBg();
    }

    /**
     * 填充画布背景
     */
    protected function createBg()
    {
        $this->bg = imagecreatetruecolor($this->bg_w, $this->bg_h);
        $c = imagecolorallocate($this->bg, $this->bg_color[0], $this->bg_color[1], $this->bg_color[2]);
        imagefill($this->bg, 0, 0, $c);
    }
    /**
     * 添加图片
     * @param $img_path string 图片路径
     * @param $xy array 坐标[x坐标，y坐标]
     * @param $size_wh array 尺寸[width, height]
     */
    public function addImg($img_path, $xy = [0, 0], $size_wh = [100, 100])
    {
        list($l_w, $l_h) = getimagesize($img_path);
        $img = $this->createImageFromFile($img_path);
        imagecopyresized($this->bg, $img, $xy[0], $xy[1], 0, 0, $size_wh[0], $size_wh[1], $l_w, $l_h);
        imagedestroy($img);
        return $this;
    }

    /**
     * 填充图片 自动剪裁 图片不变形
     * @param $img_path string 图片路径
     * @param bool $b_w 背景宽度
     * @param bool $b_h 背景高度
     * @return $this
     */
    public function addImgAuto($img_path ,$b_w = false , $b_h = false)
    {
        list($l_w, $l_h) = getimagesize($img_path);
        $b_w == false && $b_w = $this->bg_w;
        $b_h == false && $b_h = $this->bg_h;
        list($desCutPos_x , $desCutPos_y, $thumbw , $thumbh) = $this->calc($b_w , $b_h , $l_w , $l_h);
        $img = $this->createImageFromFile($img_path);
        imagecopyresized($this->bg, $img, 0, 0,  $desCutPos_x , $desCutPos_y ,$b_w , $b_h ,$thumbw , $thumbh);
        imagedestroy($img);
        return $this;
    }

    /**
     * 添加图片位置居中
     * @param $img_path string 图片路径
     * @param array $from 开始坐标
     * @param array $to 结束 坐标
     * @return $this
     */
    public function addImgCenter($img_path ,$from = [0 , 0] , $to = [0 , 0])
    {
        list($l_w, $l_h) = getimagesize($img_path);
        $to[0] == 0 && $to[0] = $this->bg_w;
        $to[1] == 0 && $to[1] = $this->bg_h;
        // 总宽高
        $bg_w = $to[0] - $from[0] - $l_w;
        $bg_h = $to[1] - $from[1] - $l_h;
        $x = $bg_w / 2 + $from[0];
        $y = $bg_h / 2 + $from[1];
        // halt($x , $y);
        $img = $this->createImageFromFile($img_path);
        imagecopyresized($this->bg, $img, $x , $y,  0 , 0 ,$l_w , $l_h ,$l_w , $l_h);
        imagedestroy($img);
        return $this;

    }
    /**
     * 添加文字
     * @param $text string 文字
     * @param $size int 文字大小
     * @param $xy array 坐标[x坐标，y坐标]
     * @param $color array [R,G,B] color value
     * @param $font_file string 字体路径
     * @param $angle int 文字旋转角度
     */
    public function addText($text, $size = 14, $xy = [0, 0], $color = [0, 0, 0], $font_file = '', $angle = 0)
    {
        if($font_file == '') $font_file = __DIR__ . DIRECTORY_SEPARATOR . 'msyh.ttc';
        $font_color = ImageColorAllocate($this->bg, $color[0], $color[1], $color[2]);
        imagettftext($this->bg, $size, $angle, $xy[0], $xy[1], $font_color, $font_file, $text);
        return $this;
    }

    /**
     * 添加二维码
     * @param $text string 文字
     * @param $xy array 坐标[x坐标，y坐标]
     * @param $size_wh int 宽度
     * @param bool $label 二维码下面文字
     * @param int $labelSize 文字大小
     * @return PosterMaker
     */
    public function addQrCode($text, $xy = [0, 0], $size_wh = 100 , $margin = 10 , $label = false , $labelSize = 12)
    {
        if (!is_readable('./tempqr')) mkdir('./tempqr', 0700);
        $tmp_name = './tempqr/' . uniqid() . '.png';
        $this->_qrCode($text, $tmp_name , $size_wh , $margin , $label , $labelSize);

        return $this->addImg($tmp_name, $xy, [$size_wh , $size_wh]);
    }

    public function addQrCodeCenter($text, $size_wh = 100 , $margin = 10 , $label = false , $labelSize = 12 ,$from = [0 , 0] , $to = [0 , 0])
    {
        if (!is_readable('./tempqr')) mkdir('./tempqr', 0700);
        $tmp_name = './tempqr/' . uniqid() . '.png';
        $this->_qrCode($text, $tmp_name , $size_wh , $margin , $label , $labelSize);
        $this->addImgCenter($tmp_name ,$from , $to);

    }
    /**
     * 输出图片
     * @param $file_name string 最后保存海报的路径，留空表示直接向浏览器输出图片
     */
    public function render($file_name='')
    {
        if ($file_name != '') {
            imagepng($this->bg, $file_name);
        } else {
            Header("Content-Type: image/png");
            imagepng($this->bg);
        }
        imagedestroy($this->bg);
    }
    /**
     * 从图片文件创建Image资源
     * @param $file 图片文件，支持url
     * @return bool|resource   成功返回图片image资源，失败返回false
     */
    public function createImageFromFile($file)
    {
        if (preg_match('/http(s)?:\/\//', $file)) {
            $fileSuffix = $this->getNetworkImgType($file);
        } else {
            $fileSuffix = pathinfo($file, PATHINFO_EXTENSION);
        }
        if (!$fileSuffix) return false;
        switch ($fileSuffix) {
            case 'jpeg':
                $theImage = @imagecreatefromjpeg($file);
                break;
            case 'jpg':
                $theImage = @imagecreatefromjpeg($file);
                break;
            case 'png':
                $theImage = @imagecreatefrompng($file);
                break;
            case 'gif':
                $theImage = @imagecreatefromgif($file);
                break;
            default:
                $theImage = @imagecreatefromstring(file_get_contents($file));
                break;
        }
        return $theImage;
    }
    /**
     * 获取网络图片类型
     * @param $url  网络图片url,支持不带后缀名url
     * @return bool
     */
    public function getNetworkImgType($url)
    {
        $ch = curl_init(); //初始化curl
        curl_setopt($ch, CURLOPT_URL, $url); //设置需要获取的URL
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3); //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //支持https
        curl_exec($ch); // 执行curl会话
        $http_code = curl_getinfo($ch); //获取curl连接资源句柄信息
        curl_close($ch); // 关闭资源连接
        if ($http_code['http_code'] == 200) {
            $theImgType = explode('/', $http_code['content_type']);
            if ($theImgType[0] == 'image') {
                return $theImgType[1];
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 通过画布宽高 原图像宽高 计算按画布尺寸缩放
     * @param $x float 画布
     * @param $y float 画布
     * @param $imgWidth float 原图像
     * @param $imgHeight float 原图像
     * @return array [裁剪位置x,y,图像宽高 x,y]
     */
    protected function calc($x , $y ,$imgWidth , $imgHeight)
    {
        //计算缩放因子
        $Des_scale = $imgWidth / $imgHeight; //原图像宽高比
        $Origin_scale = $x / $y; //画布宽高比
        if ($Des_scale>$Origin_scale) //画布宽高比小于画布宽高比，画布的宽度较大
        {
            $thumbh = $imgHeight;
            $thumbw = $imgHeight / $y * $x;
            //裁切位置 缩放后的宽减去布布宽/2 * 原图像宽高比  计算出原图像x开始点
            $desCutPos_x = ($imgWidth-$thumbw)/2;
            $desCutPos_y = 0;
        } else {
            $thumbw = $imgWidth;
            $thumbh = $imgWidth / $x * $y;
            $desCutPos_x = 0;
            $desCutPos_y = ($imgHeight-$thumbh)/2;
        }

        return [$desCutPos_x , $desCutPos_y, $thumbw , $thumbh ];
    }

    private function _qrCode($text, $file_path, $size_wh, $margin, $label, $labelSize)
    {
        $api = new \Endroid\QrCode\QrCode();
        $api->setSize($size_wh);
        $api->setMargin($margin);
        if ($label){
            $api->setLabel($label , $labelSize);
        }

        $api->setText($text);
        $api->writeFile($file_path);
    }

}